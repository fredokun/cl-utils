# cl-utils

This repository contains a single file of various *personal* utilities
I exploit in almost all my Common Lisp projects, but which doesn't
really worth the creation of a quicklisp package used by roughtly one person
on earth.

The file is intended to be copied verbatim and used as a component inside
other projects.

**Important** : the utilities in this file have no dependency beyond the
Common Lisp standard.

----

**Copyright** © 2023 Frederic Peschanski under the MIT License
(cf. LICENSE).
